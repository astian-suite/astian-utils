package org.astiansuite.utils.samples.activities

import android.os.Bundle
import org.astiansuite.utils.activities.BaseSimpleActivity
import org.astiansuite.utils.dialogs.BottomSheetChooserDialog
import org.astiansuite.utils.extensions.appLaunched
import org.astiansuite.utils.extensions.toast
import org.astiansuite.utils.extensions.viewBinding
import org.astiansuite.utils.helpers.LICENSE_AUTOFITTEXTVIEW
import org.astiansuite.utils.models.FAQItem
import org.astiansuite.utils.models.SimpleListItem
import org.astiansuite.utils.samples.BuildConfig
import org.astiansuite.utils.samples.R
import org.astiansuite.utils.samples.databinding.ActivityMainBinding

class MainActivity : BaseSimpleActivity() {
    override fun getAppLauncherName() = getString(R.string.smtco_app_name)

    override fun getAppIconIDs(): ArrayList<Int> {
        val ids = ArrayList<Int>()
        ids.add(R.mipmap.commons_launcher)
        return ids
    }

    private val binding by viewBinding(ActivityMainBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        appLaunched(BuildConfig.APPLICATION_ID)

        updateMaterialActivityViews(binding.mainCoordinator, binding.mainHolder, useTransparentNavigation = true, useTopSearchMenu = false)
        setupMaterialScrollListener(binding.mainNestedScrollview, binding.mainToolbar)

        binding.mainColorCustomization.setOnClickListener {
            startCustomizationActivity()
        }
        binding.bottomSheetChooser.setOnClickListener {
            launchAbout()
        }
    }

    private fun launchAbout() {
        val licenses = LICENSE_AUTOFITTEXTVIEW

        val faqItems = arrayListOf(
            FAQItem(org.astiansuite.utils.R.string.faq_1_title_commons, org.astiansuite.utils.R.string.faq_1_text_commons),
            FAQItem(org.astiansuite.utils.R.string.faq_1_title_commons, org.astiansuite.utils.R.string.faq_1_text_commons),
            FAQItem(org.astiansuite.utils.R.string.faq_4_title_commons, org.astiansuite.utils.R.string.faq_4_text_commons)
        )

        if (!resources.getBoolean(org.astiansuite.utils.R.bool.hide_google_relations)) {
            faqItems.add(FAQItem(org.astiansuite.utils.R.string.faq_2_title_commons, org.astiansuite.utils.R.string.faq_2_text_commons))
            faqItems.add(FAQItem(org.astiansuite.utils.R.string.faq_6_title_commons, org.astiansuite.utils.R.string.faq_6_text_commons))
        }

        startAboutActivity(R.string.smtco_app_name, licenses, BuildConfig.VERSION_NAME, faqItems, true)
    }

    private fun launchBottomSheetDemo() {
        BottomSheetChooserDialog.createChooser(
            fragmentManager = supportFragmentManager,
            title = org.astiansuite.utils.R.string.please_select_destination,
            items = arrayOf(
                SimpleListItem(1, org.astiansuite.utils.R.string.record_video, org.astiansuite.utils.R.drawable.ic_camera_vector),
                SimpleListItem(
                    2,
                    org.astiansuite.utils.R.string.record_audio,
                    org.astiansuite.utils.R.drawable.ic_microphone_vector,
                    selected = true
                ),
                SimpleListItem(4, org.astiansuite.utils.R.string.choose_contact, org.astiansuite.utils.R.drawable.ic_add_person_vector)
            )
        ) {
            toast("Clicked ${it.id}")
        }
    }

    override fun onResume() {
        super.onResume()
        setupToolbar(binding.mainToolbar)
    }
}
