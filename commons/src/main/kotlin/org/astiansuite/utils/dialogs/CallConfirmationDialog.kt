package org.astiansuite.utils.dialogs

import android.view.animation.AnimationUtils
import org.astiansuite.utils.R
import org.astiansuite.utils.activities.BaseSimpleActivity
import org.astiansuite.utils.databinding.DialogCallConfirmationBinding
import org.astiansuite.utils.extensions.applyColorFilter
import org.astiansuite.utils.extensions.getAlertDialogBuilder
import org.astiansuite.utils.extensions.getProperTextColor
import org.astiansuite.utils.extensions.setupDialogStuff

class CallConfirmationDialog(val activity: BaseSimpleActivity, val callee: String, private val callback: () -> Unit) {
    private var view = DialogCallConfirmationBinding.inflate(activity.layoutInflater, null, false)

    init {
        view.callConfirmPhone.applyColorFilter(activity.getProperTextColor())
        activity.getAlertDialogBuilder()
            .setNegativeButton(R.string.cancel, null)
            .apply {
                val title = String.format(activity.getString(R.string.confirm_calling_person), callee)
                activity.setupDialogStuff(view.root, this, titleText = title) { alertDialog ->
                    view.callConfirmPhone.apply {
                        startAnimation(AnimationUtils.loadAnimation(activity, R.anim.shake_pulse_animation))
                        setOnClickListener {
                            callback.invoke()
                            alertDialog.dismiss()
                        }
                    }
                }
            }
    }
}
