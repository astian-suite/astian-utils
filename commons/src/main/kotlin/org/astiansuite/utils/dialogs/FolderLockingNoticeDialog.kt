package org.astiansuite.utils.dialogs

import android.app.Activity
import org.astiansuite.utils.R
import org.astiansuite.utils.databinding.DialogTextviewBinding
import org.astiansuite.utils.extensions.baseConfig
import org.astiansuite.utils.extensions.getAlertDialogBuilder
import org.astiansuite.utils.extensions.setupDialogStuff

class FolderLockingNoticeDialog(val activity: Activity, val callback: () -> Unit) {
    init {
        val view = DialogTextviewBinding.inflate(activity.layoutInflater, null, false).apply {
            textView.text = activity.getString(R.string.lock_folder_notice)
        }

        activity.getAlertDialogBuilder()
            .setPositiveButton(R.string.ok) { _, _ -> dialogConfirmed() }
            .setNegativeButton(R.string.cancel, null)
            .apply {
                activity.setupDialogStuff(view.root, this, R.string.disclaimer)
            }
    }

    private fun dialogConfirmed() {
        activity.baseConfig.wasFolderLockingNoticeShown = true
        callback()
    }
}
