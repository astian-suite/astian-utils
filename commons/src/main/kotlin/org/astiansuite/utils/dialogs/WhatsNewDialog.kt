package org.astiansuite.utils.dialogs

import android.app.Activity
import android.view.LayoutInflater
import org.astiansuite.utils.R
import org.astiansuite.utils.databinding.DialogWhatsNewBinding
import org.astiansuite.utils.extensions.getAlertDialogBuilder
import org.astiansuite.utils.extensions.setupDialogStuff
import org.astiansuite.utils.models.Release

class WhatsNewDialog(val activity: Activity, val releases: List<Release>) {
    init {
        val view = DialogWhatsNewBinding.inflate(LayoutInflater.from(activity), null, false)
        view.whatsNewContent.text = getNewReleases()

        activity.getAlertDialogBuilder()
            .setPositiveButton(R.string.ok, null)
            .apply {
                activity.setupDialogStuff(view.root, this, R.string.whats_new, cancelOnTouchOutside = false)
            }
    }

    private fun getNewReleases(): String {
        val sb = StringBuilder()

        releases.forEach {
            val parts = activity.getString(it.textId).split("\n").map(String::trim)
            parts.forEach {
                sb.append("- $it\n")
            }
        }

        return sb.toString()
    }
}
