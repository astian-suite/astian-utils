package org.astiansuite.utils.dialogs

import org.astiansuite.utils.R
import org.astiansuite.utils.activities.BaseSimpleActivity
import org.astiansuite.utils.databinding.DialogOpenDeviceSettingsBinding
import org.astiansuite.utils.extensions.getAlertDialogBuilder
import org.astiansuite.utils.extensions.openDeviceSettings
import org.astiansuite.utils.extensions.setupDialogStuff

class OpenDeviceSettingsDialog(val activity: BaseSimpleActivity, message: String) {

    init {
        activity.apply {
            val view = DialogOpenDeviceSettingsBinding.inflate(layoutInflater, null, false)
            view.openDeviceSettings.text = message
            getAlertDialogBuilder()
                .setNegativeButton(R.string.close, null)
                .setPositiveButton(R.string.go_to_settings) { _, _ ->
                    openDeviceSettings()
                }.apply {
                    setupDialogStuff(view.root, this)
                }
        }
    }
}
