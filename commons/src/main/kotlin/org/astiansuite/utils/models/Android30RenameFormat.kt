package org.astiansuite.utils.models

enum class Android30RenameFormat {
    SAF,
    CONTENT_RESOLVER,
    NONE
}
