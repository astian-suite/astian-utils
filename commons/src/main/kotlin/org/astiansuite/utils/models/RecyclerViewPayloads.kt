package org.astiansuite.utils.models

data class RecyclerSelectionPayload(val selected: Boolean)
