package org.astiansuite.utils.models

data class Release(val id: Int, val textId: Int)
