package org.astiansuite.utils.interfaces

interface RefreshRecyclerViewListener {
    fun refreshItems()
}
