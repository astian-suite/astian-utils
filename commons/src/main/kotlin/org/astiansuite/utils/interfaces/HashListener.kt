package org.astiansuite.utils.interfaces

interface HashListener {
    fun receivedHash(hash: String, type: Int)
}
