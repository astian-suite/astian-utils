package org.astiansuite.utils.interfaces

interface LineColorPickerListener {
    fun colorChanged(index: Int, color: Int)
}
