package org.astiansuite.utils.interfaces

interface RecyclerScrollCallback {
    fun onScrolled(scrollY: Int)
}
