package org.astiansuite.utils.compose.extensions

import android.content.Context
import org.astiansuite.utils.helpers.BaseConfig

val Context.config: BaseConfig get() = BaseConfig.newInstance(applicationContext)
