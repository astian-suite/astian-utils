package org.astiansuite.utils.extensions

import android.content.Context
import org.astiansuite.utils.models.FileDirItem

fun FileDirItem.isRecycleBinPath(context: Context): Boolean {
    return path.startsWith(context.recycleBinPath)
}
