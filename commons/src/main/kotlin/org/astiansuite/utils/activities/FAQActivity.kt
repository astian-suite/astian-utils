package org.astiansuite.utils.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.remember
import androidx.core.view.WindowCompat
import org.astiansuite.utils.compose.extensions.TransparentSystemBars
import org.astiansuite.utils.compose.screens.FAQScreen
import org.astiansuite.utils.compose.theme.AppThemeSurface
import org.astiansuite.utils.helpers.APP_FAQ
import org.astiansuite.utils.models.FAQItem
import kotlinx.collections.immutable.toImmutableList

class FAQActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            TransparentSystemBars()
            AppThemeSurface {
                val faqItems = remember { intent.getSerializableExtra(APP_FAQ) as ArrayList<FAQItem> }
                FAQScreen(goBack = ::finish, faqItems = faqItems.toImmutableList())
            }
        }
    }
}
